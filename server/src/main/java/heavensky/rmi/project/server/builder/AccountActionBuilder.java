package heavensky.rmi.project.server.builder;

import heavensky.rmi.project.server.interfaces.IAccountActionBuilder;
import heavensky.rmi.project.server.model.AccountModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountActionBuilder implements IAccountActionBuilder {

    private String getAccount;
    private String findAccountByName;


    @Override
    public IAccountActionBuilder getAccount(String actionName) {
        this.getAccount = actionName;
        return this;
    }

    @Override
    public IAccountActionBuilder findAccountByNameString(String actionName) {
        this.findAccountByName = actionName;
        return this;
    }

    @Override
    public AccountModel build() {
        return new AccountModel(getAccount, findAccountByName);
    }
}
