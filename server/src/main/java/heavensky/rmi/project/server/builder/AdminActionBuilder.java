package heavensky.rmi.project.server.builder;

import heavensky.rmi.project.server.interfaces.IAdminActionBuilder;
import heavensky.rmi.project.server.model.AdminModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminActionBuilder implements IAdminActionBuilder {

    private String getAdmin;

    @Override
    public AdminActionBuilder getAdmin(String actionName) {
        this.getAdmin = actionName;
        return this;
    }

    @Override
    public AdminModel build() {
        return new AdminModel(getAdmin);
    }
}
