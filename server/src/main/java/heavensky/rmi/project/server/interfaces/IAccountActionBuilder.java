package heavensky.rmi.project.server.interfaces;

import heavensky.rmi.project.server.model.AccountModel;

public interface IAccountActionBuilder {
    IAccountActionBuilder getAccount(String actionName);

    IAccountActionBuilder findAccountByNameString(String actionName);

    AccountModel build();
}
