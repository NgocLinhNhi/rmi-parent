package heavensky.rmi.project.server.service;

import heavensky.rmi.config.project.entity.Admin;
import heavensky.rmi.config.project.interfaces.IAdmin;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class AdminServiceImpl extends UnicastRemoteObject implements IAdmin {
    private static final long serialVersionUID = 1L;

    public AdminServiceImpl() throws RemoteException {
    }

    public Admin getAdmin() {
        Admin admin = new Admin();
        admin.setUserName("Viet");
        admin.setAge(35);
        admin.setPassword("1987");
        return admin;
    }
}