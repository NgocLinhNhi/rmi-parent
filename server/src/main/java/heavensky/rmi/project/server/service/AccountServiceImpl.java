package heavensky.rmi.project.server.service;

import heavensky.rmi.config.project.entity.User;
import heavensky.rmi.config.project.interfaces.IAccount;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class AccountServiceImpl extends UnicastRemoteObject implements IAccount {
    private static final long serialVersionUID = 1L;

    public AccountServiceImpl() throws RemoteException {
    }

    @Override
    public User getUser() {
        User user = new User();
        user.setUserName("Ninh");
        user.setAge(32);
        user.setPassword("1991");
        return user;
    }

    @Override
    public User findUserByName(String name) {
        if (name.equals("Ninh")) return getUser();
        else return null;
    }
}