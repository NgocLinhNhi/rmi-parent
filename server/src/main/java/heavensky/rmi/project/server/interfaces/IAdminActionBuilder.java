package heavensky.rmi.project.server.interfaces;

import heavensky.rmi.project.server.model.AdminModel;

public interface IAdminActionBuilder {
    IAdminActionBuilder getAdmin(String actionName);

    AdminModel build();
}
