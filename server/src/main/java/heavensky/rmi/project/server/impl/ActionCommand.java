package heavensky.rmi.project.server.impl;

import heavensky.rmi.config.project.interfaces.IRemote;
import heavensky.rmi.project.server.interfaces.IServerCommand;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class ActionCommand implements IServerCommand {

    private final Map<String, IRemote> handlers = new HashMap<>();

    private static ActionCommand INSTANCE;

    public static ActionCommand getInstance() {
        if (INSTANCE == null) INSTANCE = new ActionCommand();
        return INSTANCE;
    }

    public void addActionServerName(Set<String> actionName, IRemote iRemote) {
        for (String name : actionName) {
            addServerRemote(name, iRemote);
        }
    }

    @Override
    public void addServerRemote(String actionName, IRemote iRemote) {
        handlers.put(actionName, iRemote);
    }

}
