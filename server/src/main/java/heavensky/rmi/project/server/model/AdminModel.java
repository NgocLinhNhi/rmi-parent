package heavensky.rmi.project.server.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class AdminModel {
    public Set<String> listAdminAction = new HashSet<>();

    public AdminModel(String getAdmin) {
        listAdminAction.add(getAdmin);
    }
}
