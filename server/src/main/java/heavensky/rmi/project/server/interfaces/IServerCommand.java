package heavensky.rmi.project.server.interfaces;


import heavensky.rmi.config.project.interfaces.IRemote;

public interface IServerCommand {
    void addServerRemote(String actionName, IRemote handler);

}
