package heavensky.rmi.project.server;

import heavensky.rmi.project.server.handler.ServerRMIHandler;

public class ServerRMIStartUp {

    public static void main(String[] args) {
        ServerRMIHandler.getInstance().start();
    }
}
