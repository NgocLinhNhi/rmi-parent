package heavensky.rmi.project.server.handler;

import heavensky.rmi.config.project.config.ServerRMIConfiguration;
import heavensky.rmi.config.project.interfaces.IAccount;
import heavensky.rmi.project.server.impl.ActionCommand;
import heavensky.rmi.project.server.builder.AccountActionBuilder;
import heavensky.rmi.project.server.builder.AdminActionBuilder;
import heavensky.rmi.project.server.service.AccountServiceImpl;
import heavensky.rmi.project.server.service.AdminServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Set;

import static heavensky.rmi.config.project.constant.RMIConstant.*;

public class ServerRMIHandler {
    private static final Logger logger = LoggerFactory.getLogger(ServerRMIHandler.class);

    private static ServerRMIHandler INSTANCE;

    public static ServerRMIHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new ServerRMIHandler();
        return INSTANCE;
    }

    public void start() {
        ServerRMIConfiguration serverRMIConfiguration = ServerRMIConfiguration.getInstance();
        ActionCommand actionCommand = ActionCommand.getInstance();

        try {
            initServer(serverRMIConfiguration);
            Set<String> lstAccountAction = buildListAccountAction();

            //Add AccountModel /AdminModel Server
            actionCommand.addActionServerName(lstAccountAction, buildAccountService());
            actionCommand.addActionServerName(buildAdminActionName(), buildAdminService());

            //call config build RMI server
            serverRMIConfiguration.buildNamingServer(actionCommand.getHandlers());

        } catch (Exception e) {
            logger.error("Remote has exception {1}", e);
        }
    }

    private void initServer(ServerRMIConfiguration instance) throws Exception {
        instance.initRMIServerConfig();
    }

    private Set<String> buildListAccountAction() {
        return new AccountActionBuilder()
                .getAccount(GET_ACCOUNT)
                .findAccountByNameString(FIND_ACCOUNT_BY_NAME)
                .build()
                .getListAccountAction();
    }

    private Set<String> buildAdminActionName() {
        return new AdminActionBuilder()
                .getAdmin(GET_ADMIN)
                .build()
                .getListAdminAction();
    }

    private IAccount buildAccountService() throws RemoteException {
        return new AccountServiceImpl();
    }

    private AdminServiceImpl buildAdminService() throws RemoteException {
        return new AdminServiceImpl();
    }
}
