package heavensky.rmi.project.server.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class AccountModel {

    Set<String> listAccountAction = new HashSet<>();

    public AccountModel(String getAccount, String findAccountByName) {
        listAccountAction.add(getAccount);
        listAccountAction.add(findAccountByName);
    }
}
