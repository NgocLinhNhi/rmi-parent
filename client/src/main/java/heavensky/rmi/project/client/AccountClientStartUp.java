package heavensky.rmi.project.client;

import heavensky.rmi.project.client.action.AccountCommand;
import heavensky.rmi.project.client.handler.AccountClientHandler;
import heavensky.rmi.project.client.interfaces.ICommand;
import heavensky.rmi.project.client.service.AccountService;

//Test push
public class AccountClientStartUp {

    public static void main(String[] args) throws Exception {
        ICommand command = new AccountCommand(new AccountService());
        AccountClientHandler accountClientHandler = new AccountClientHandler(command);
        accountClientHandler.getAccount();
        accountClientHandler.findAccountByName("Ninh");
    }
}
