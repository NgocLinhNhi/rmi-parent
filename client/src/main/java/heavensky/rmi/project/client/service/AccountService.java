package heavensky.rmi.project.client.service;

import heavensky.rmi.config.project.config.LoadPropertiesFile;
import heavensky.rmi.config.project.entity.User;
import heavensky.rmi.config.project.interfaces.IAccount;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import static heavensky.rmi.config.project.constant.RMIConstant.FIND_ACCOUNT_BY_NAME;
import static heavensky.rmi.config.project.constant.RMIConstant.GET_ACCOUNT;

public class AccountService {
    private String clientIp;

    public AccountService() throws Exception {
        this.clientIp = LoadPropertiesFile.getInstance().loadProperties().getProperty("client.rmi.ip");
    }

    public User getAccount() throws RemoteException, MalformedURLException, NotBoundException {
        IAccount getAccount = (IAccount) Naming.lookup(clientIp + GET_ACCOUNT);
        return getAccount.getUser();
    }

    public User findAccountByName(String name) throws RemoteException, NotBoundException, MalformedURLException {
        IAccount findAccount = (IAccount) Naming.lookup(clientIp + FIND_ACCOUNT_BY_NAME);
        return findAccount.findUserByName(name);
    }

}
