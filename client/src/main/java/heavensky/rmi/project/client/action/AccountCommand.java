package heavensky.rmi.project.client.action;

import heavensky.rmi.config.project.entity.User;
import heavensky.rmi.project.client.interfaces.ICommand;
import heavensky.rmi.project.client.service.AccountService;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AccountCommand implements ICommand {
    private AccountService accountService;

    public AccountCommand(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public User getUser() throws RemoteException, NotBoundException, MalformedURLException {
        return accountService.getAccount();
    }

    @Override
    public User findUserByName(String name) throws RemoteException, MalformedURLException, NotBoundException {
        return accountService.findAccountByName(name);
    }

}
