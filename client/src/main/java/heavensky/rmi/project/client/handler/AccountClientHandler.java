package heavensky.rmi.project.client.handler;

import heavensky.rmi.config.project.entity.User;
import heavensky.rmi.project.client.interfaces.ICommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AccountClientHandler {
    private static final Logger logger = LoggerFactory.getLogger(AccountClientHandler.class);

    private ICommand accountCommand;

    public AccountClientHandler(ICommand accountCommand) {
        this.accountCommand = accountCommand;
    }

    public void getAccount() {
        try {
            User user = accountCommand.getUser();
            logger.info(" Name :  " + user.getUserName());

//            IAdmin iAdmin = (IAdmin) Naming.lookup("rmi://192.168.1.3:6789/getAdmin");
//            logger.info(" Name :  " + iAdmin.getAdmin().getUserName());

        } catch (NotBoundException e) {
            logger.error("NotBoundException {1}", e);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException has exception {1}", e);
        } catch (RemoteException e) {
            logger.error("Remote has exception {1}", e);
        }
    }

    public void findAccountByName(String name) {
        try {
            User user = accountCommand.findUserByName(name);
            logger.info(" Name has been find :  " + user.getUserName());
        } catch (NotBoundException e) {
            logger.error("NotBoundException {1}", e);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException has exception {1}", e);
        } catch (RemoteException e) {
            logger.error("Remote has exception {1}", e);
        }
    }
}