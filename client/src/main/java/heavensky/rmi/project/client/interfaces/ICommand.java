package heavensky.rmi.project.client.interfaces;

import heavensky.rmi.config.project.entity.User;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public interface ICommand {

    User getUser() throws RemoteException, MalformedURLException, NotBoundException;

    User findUserByName(String name) throws RemoteException, MalformedURLException, NotBoundException;

}
