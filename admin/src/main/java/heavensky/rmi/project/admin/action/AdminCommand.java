package heavensky.rmi.project.admin.action;

import heavensky.rmi.config.project.entity.Admin;
import heavensky.rmi.project.admin.interfaces.ICommand;
import heavensky.rmi.project.admin.service.AdminService;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AdminCommand implements ICommand {
    private AdminService adminService;

    public AdminCommand(AdminService adminService) {
        this.adminService = adminService;
    }

    @Override
    public Admin getAdmin() throws RemoteException, NotBoundException, MalformedURLException {
        return adminService.getAdmin();
    }

}
