package heavensky.rmi.project.admin.interfaces;

import heavensky.rmi.config.project.entity.Admin;
import heavensky.rmi.config.project.entity.User;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public interface ICommand {

    Admin getAdmin() throws RemoteException, MalformedURLException, NotBoundException;

}
