package heavensky.rmi.project.admin;

import heavensky.rmi.project.admin.action.AdminCommand;
import heavensky.rmi.project.admin.handler.AdminClientHandler;
import heavensky.rmi.project.admin.interfaces.ICommand;
import heavensky.rmi.project.admin.service.AdminService;

public class AdminClientStartUp {

    public static void main(String[] args) throws Exception {
        ICommand command = new AdminCommand(new AdminService());
        AdminClientHandler adminClientHandler = new AdminClientHandler(command);
        adminClientHandler.getAdmin();
    }
}
