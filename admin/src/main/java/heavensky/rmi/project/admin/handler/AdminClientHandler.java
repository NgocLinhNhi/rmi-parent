package heavensky.rmi.project.admin.handler;

import heavensky.rmi.config.project.entity.Admin;
import heavensky.rmi.project.admin.interfaces.ICommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AdminClientHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdminClientHandler.class);

    private ICommand adminCommand;

    public AdminClientHandler(ICommand adminCommand) {
        this.adminCommand = adminCommand;
    }

    public void getAdmin() {
        try {
            Admin admin = adminCommand.getAdmin();
            logger.info(" Admin Name :  " + admin.getUserName());

        } catch (NotBoundException e) {
            logger.error("NotBoundException {1}", e);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException has exception {1}", e);
        } catch (RemoteException e) {
            logger.error("Remote has exception {1}", e);
        }
    }

}