package heavensky.rmi.project.admin.service;

import heavensky.rmi.config.project.config.LoadPropertiesFile;
import heavensky.rmi.config.project.entity.Admin;
import heavensky.rmi.config.project.entity.User;
import heavensky.rmi.config.project.interfaces.IAdmin;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import static heavensky.rmi.config.project.constant.RMIConstant.GET_ADMIN;

public class AdminService {
    private String clientIp;

    public AdminService() throws Exception {
        this.clientIp = LoadPropertiesFile.getInstance().loadProperties().getProperty("client.rmi.ip");
    }

    public Admin getAdmin() throws RemoteException, MalformedURLException, NotBoundException {
        IAdmin getAdmin = (IAdmin) Naming.lookup(clientIp + GET_ADMIN);
        return getAdmin.getAdmin();
    }
}
