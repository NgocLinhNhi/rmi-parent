package heavensky.rmi.config.project.interfaces;

import heavensky.rmi.config.project.entity.Admin;

import java.rmi.RemoteException;

//extends lib Remote => để có thể gọi method của server từ client
public interface IAdmin extends IRemote {
    Admin getAdmin() throws RemoteException;
}
