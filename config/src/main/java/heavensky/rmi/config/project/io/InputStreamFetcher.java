package heavensky.rmi.config.project.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
