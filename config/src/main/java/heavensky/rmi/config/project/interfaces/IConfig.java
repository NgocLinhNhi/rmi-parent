package heavensky.rmi.config.project.interfaces;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Map;

public interface IConfig<T> {

    void initRMIServerConfig() throws Exception;

    void start() throws Exception;

    void stop(String actionName, T iAccount) throws RemoteException, MalformedURLException, NotBoundException;

    void buildNamingServer(Map<String, IRemote> mapAccount);

}
