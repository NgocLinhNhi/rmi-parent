package heavensky.rmi.config.project.constant;

public class RMIConstant {

    public static final String GET_ACCOUNT = "getAccount";
    public static final String FIND_ACCOUNT_BY_NAME = "findAccountByName";
    public static final String GET_ADMIN = "getAdmin";

}
