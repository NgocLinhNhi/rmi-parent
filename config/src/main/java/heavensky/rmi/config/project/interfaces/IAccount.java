package heavensky.rmi.config.project.interfaces;

import heavensky.rmi.config.project.entity.User;

import java.rmi.RemoteException;

//extends lib Remote => để có thể gọi method của server từ client
public interface IAccount extends IRemote {
    User getUser() throws RemoteException;

    User findUserByName(String name) throws RemoteException;

}
