package heavensky.rmi.config.project.config;

import heavensky.rmi.config.project.interfaces.IConfig;
import heavensky.rmi.config.project.interfaces.IRemote;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;

@Getter
@Setter
public class ServerRMIConfiguration implements IConfig<IRemote> {
    private final Logger logger = LoggerFactory.getLogger(ServerRMIConfiguration.class);

    private String ip;
    private String port;
    private String serverName;

    private static ServerRMIConfiguration INSTANCE;

    public static ServerRMIConfiguration getInstance() {
        if (INSTANCE == null) INSTANCE = new ServerRMIConfiguration();
        return INSTANCE;
    }

    private ServerRMIConfiguration() {
    }

    @Override
    public void initRMIServerConfig() throws Exception {
        loadProxy();
        start();
    }

    @Override
    public void start() throws Exception {
        LocateRegistry.createRegistry(Integer.valueOf(port));
    }

    @Override
    public void stop(String actionName, IRemote iRemote)
            throws RemoteException, MalformedURLException, NotBoundException {
        // Unregister ourself
        Naming.unbind(serverName);
        // this will also remove us from the RMI runtime
        UnicastRemoteObject.unexportObject(iRemote, true);
        logger.info("Server RMI has been stop !!!");
    }

    @Override
    public void buildNamingServer(Map<String, IRemote> mapAccount) {
        mapAccount.forEach((actionName, iRemote) -> {
            buildServerName(actionName);
            try {
                Naming.rebind(serverName, iRemote);
            } catch (RemoteException | MalformedURLException e) {
                logger.error("Remote to server has error {1}", e);
            }
            logger.info("Server RMI started with Name {} !!!", serverName);
        });
    }

    private void loadProxy() throws Exception {
        ip = LoadPropertiesFile.getInstance().loadProperties().getProperty("rmi.server.ip");
        port = LoadPropertiesFile.getInstance().loadProperties().getProperty("rmi.server.port");
    }

    private void buildServerName(String actionName) {
        serverName = ip + port + "/" + actionName;
        logger.info("Server Name {}", serverName);
    }
}
