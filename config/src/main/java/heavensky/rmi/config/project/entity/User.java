package heavensky.rmi.config.project.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class User implements Serializable {

    private String userName;
    private int age;
    private String password;

}
